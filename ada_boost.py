from sklearn.ensemble import AdaBoostClassifier
from get_data import get_data


def ada_boost(n_estimators, learning_rate):
    x_train, y_train, x_test, y_test = get_data()

    boost = AdaBoostClassifier(
        n_estimators=n_estimators,
        learning_rate=learning_rate,
    )
    boost.fit(x_train, y_train)

    training_score = boost.score(x_train, y_train)
    predict_result = boost.predict(x_test)
    testing_score = boost.score(x_test, y_test)
    return training_score, testing_score, predict_result
