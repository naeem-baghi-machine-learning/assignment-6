from sklearn.tree import DecisionTreeClassifier
from get_data import get_data


def decision_tree(max_depth, min_samples_split, min_samples_leaf):
    x_train, y_train, x_test, y_test = get_data()

    tree = DecisionTreeClassifier(
        max_depth=max_depth,
        min_samples_split=min_samples_split,
        min_samples_leaf=min_samples_leaf,
        random_state=0
    )
    tree.fit(x_train, y_train)

    training_score = tree.score(x_train, y_train)
    predict_result = tree.predict(x_test)
    testing_score = tree.score(x_test, y_test)
    return training_score, testing_score, predict_result
