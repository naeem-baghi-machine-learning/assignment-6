import pandas as pd


def get_data():
    headers = ['age', 'workclass', 'fnlwgt', 'education', 'education-num', 'marital-status',
               'occupation', 'relationship', 'race', 'sex', 'capital-gain', 'capital-loss',
               'hours-per-week', 'country', 'class_name']

    data_train = pd.read_csv('input/adult.data', header=None, names=headers, na_values="?")
    data_test = pd.read_csv('input/adult.test', header=None, names=headers, na_values="?")
    train_y = pd.get_dummies(data_train.class_name)[' >50K']
    test_y = pd.get_dummies(data_test.class_name)[' >50K.']
    train_x = data_train.drop(['class_name'], axis='columns')
    test_x = data_test.drop(['class_name'], axis='columns')
    data_x = train_x.append(test_x, ignore_index=True)

    workclass = pd.get_dummies(data_x.workclass)
    education = pd.get_dummies(data_x.education)
    marital_status = pd.get_dummies(data_x['marital-status'])
    occupation = pd.get_dummies(data_x.occupation)
    relationship = pd.get_dummies(data_x.relationship)
    race = pd.get_dummies(data_x.race)
    sex = pd.get_dummies(data_x.sex)
    country = pd.get_dummies(data_x.country)

    merged = pd.concat(
        [
            data_x,
            workclass,
            education,
            marital_status,
            occupation,
            relationship,
            race,
            sex,
            country
        ]
        , axis='columns'
    )

    dropped = merged.drop(
        [
            'workclass',
            'education',
            'marital-status',
            'occupation',
            'relationship',
            'race',
            'sex',
            'country'
         ]
        , axis='columns'
    )

    x_train = dropped.loc[:len(data_train) - 1]
    x_test = dropped.loc[len(data_train):]
    return x_train, train_y, x_test, test_y
