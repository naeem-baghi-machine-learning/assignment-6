from random_forest import random_forest
from decision_tree import decision_tree
from ada_boost import ada_boost
from sklearn import metrics
from get_data import get_data
from matplotlib import pyplot


def random_forest_evaluator():
    best_values = 0, 0, 0
    best_result = 0, 0
    for j in range(0, 5):
        for i in range(0, 5):
            for k in range(0, 5):
                training_result, testing_result = random_forest(n_estimators=i * 100 + 50, max_depth=j * 10 + 5,
                                                                max_features=k * 10 + 5)
                if (testing_result > best_result[1]) or \
                        (testing_result == best_result[1] and training_result > best_result[0]):
                    best_result = training_result, testing_result
                    best_values = i * 100 + 50, j * 10 + 5, k * 10 + 5
                    print('new best result {:.3f}'.format(training_result), '{:.3f}'.format(testing_result), best_values)
    print('best result: ', best_values)

    # new best result 0.809 0.809 (50, 5, 5)
    # new best result 0.845 0.845 (50, 5, 15)
    # new best result 0.850 0.849 (50, 5, 25)
    # new best result 0.851 0.851 (50, 5, 35)
    # new best result 0.853 0.852 (50, 5, 45)
    # new best result 0.854 0.853 (250, 5, 45)
    # new best result 0.874 0.857 (50, 15, 5)
    # new best result 0.894 0.864 (50, 15, 15)
    # new best result 0.901 0.864 (50, 15, 25)
    # new best result 0.906 0.866 (50, 15, 35)
    # new best result 0.902 0.867 (150, 15, 25)
    # new best result 0.909 0.867 (150, 15, 45)
    # new best result 0.907 0.868 (350, 15, 35)
    # new best result 0.907 0.868 (450, 15, 35)
    # best result: (450, 15, 35)


def decision_tree_evaluator():
    best_values = 0, 0, 0
    best_result = 0, 0
    for j in range(0, 5):
        for i in range(0, 5):
            for k in range(0, 5):
                training_result, testing_result = decision_tree(min_samples_split=i * 100 + 50, max_depth=j * 10 + 5,
                                                                min_samples_leaf=k * 10 + 5)
                if (testing_result > best_result[1]) or \
                        (testing_result == best_result[1] and training_result > best_result[0]):
                    best_result = training_result, testing_result
                    best_values = i * 100 + 50, j * 10 + 5, k * 10 + 5
                    print('new best result {:.3f}'.format(training_result), '{:.3f}'.format(testing_result),
                          best_values)
    print('best result: ', best_values)

    # new best result 0.852 0.852 (50, 5, 5)
    # new best result 0.852 0.853 (50, 5, 25)
    # new best result 0.876 0.855 (50, 15, 5)
    # new best result 0.873 0.855 (50, 15, 15)
    # new best result 0.867 0.858 (50, 15, 35)
    # new best result 0.867 0.859 (50, 15, 45)
    # new best result 0.865 0.860 (150, 15, 45)
    # new best result 0.867 0.862 (250, 15, 5)
    # new best result 0.864 0.862 (250, 15, 45)
    # best result:  (250, 15, 45)


def ada_boost_evaluator():
    best_values = 0, 0
    best_result = 0, 0
    for j in range(0, 7):
        for i in range(0, 4):
            training_result, testing_result = ada_boost(n_estimators=i * 50 + 10, learning_rate=j + 1)
            if (testing_result > best_result[1]) or \
                    (testing_result == best_result[1] and training_result > best_result[0]):
                best_result = training_result, testing_result
                best_values = i * 50 + 10, j + 1
                print('new best result {:.3f}'.format(training_result), '{:.3f}'.format(testing_result), best_values)
    print('best result: ', best_values)

    # new best result 0.852 0.854 (10, 1)
    # new best result 0.862 0.862 (60, 1)
    # new best result 0.866 0.863 (110, 1)
    # new best result 0.868 0.865 (160, 1)
    # best result:  (160, 1)


def roc_evaluator():
    _, _, _, y_test = get_data()
    _, _, dt_prediction_class = decision_tree(min_samples_split=250, max_depth=15, min_samples_leaf=45)
    fpr, tpr, _ = metrics.roc_curve(y_test, dt_prediction_class)
    pyplot.plot(fpr, tpr)
    pyplot.xlim([0.0, 1.0])
    pyplot.ylim([0.0, 1.0])
    pyplot.grid(True)
    pyplot.savefig('decision_tree.png')

    _, _, rf_prediction_class = random_forest(n_estimators=450, max_depth=15,max_features=35)
    fpr, tpr, _ = metrics.roc_curve(y_test, rf_prediction_class)
    pyplot.plot(fpr, tpr)
    pyplot.xlim([0.0, 1.0])
    pyplot.ylim([0.0, 1.0])
    pyplot.grid(True)
    pyplot.savefig('random_forest.png')

    _, _, ab_prediction_class = ada_boost(n_estimators=160, learning_rate=1)
    fpr, tpr, _ = metrics.roc_curve(y_test, ab_prediction_class)
    pyplot.plot(fpr, tpr)
    pyplot.xlim([0.0, 1.0])
    pyplot.ylim([0.0, 1.0])
    pyplot.grid(True)
    pyplot.savefig('ada_boost.png')
