from sklearn.tree import DecisionTreeClassifier
from get_data import get_data


x_train, y_train, x_test, y_test = get_data()

tree = DecisionTreeClassifier(random_state=0)
tree.fit(x_train, y_train)

print('\n\nAccuracy on the training subset: {:.3f}'.format(tree.score(x_train, y_train)))
print('Accuracy on the testing subset: {:.3f}\n'.format(tree.score(x_test, y_test)))