from sklearn.ensemble import RandomForestClassifier
from get_data import get_data


def random_forest(n_estimators, max_features, max_depth):
    x_train, y_train, x_test, y_test = get_data()

    forest = RandomForestClassifier(
        n_estimators=n_estimators,
        max_features=max_features,
        max_depth=max_depth,
        n_jobs=10
    )
    forest.fit(x_train, y_train)

    training_score = forest.score(x_train, y_train)
    predict_result = forest.predict(x_test)
    testing_score = forest.score(x_test, y_test)
    return training_score, testing_score, predict_result
